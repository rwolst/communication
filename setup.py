#!/usr/bin/env python

from distutils.core import setup
import codecs
import sys
import os
import os.path as path
import shutil


# where this file is located
cwd = path.dirname(__file__)

# setup options
setup(
    name='communication',
    license='MIT',
    description='A protocol for TCP transmission of arrays.',
    classifiers=[
        'License :: OSI Approved :: MIT License',
        'Development Status :: 5 - Production/Stable',
        'Environment :: Console',
        'Intended Audience :: Developers',
        'Intended Audience :: Other Audience',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Natural Language :: English',
        'Topic :: Other/Nonlisted Topic',
    ],
    keywords=[
        'protocol', 'TCP', 'arrays'
    ],
    platforms='ANY',
    install_requires=['numpy', 'contexttimer'],
    packages=['communication'],
)
