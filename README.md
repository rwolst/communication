# Communication
A repository containing protocols for Python and C to communicate over TCP
connection.

## Python
Can install by running

    python3 -m pip install .

and can test by installing `pytest` i.e.

    python3 -m pip install pytest
    python3 -m pytest communication

## C++
Can check the C++ programs using

    g++ -g -Wall main.cpp protocol.cpp -o main

then in separate shells running for example

    ./main server int32 float32

and

    ./main client int32 float32
