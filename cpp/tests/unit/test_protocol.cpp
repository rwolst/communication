// Copyright [2019] Rob

#include <pthread.h>
#include <assert.h>
#include <vector>
#include <thread>
#include <chrono>
#include <queue>
#include <random>
#include "../../protocol.hpp"


std::string HOST = "127.0.0.1";
int PORT = 65432;

std::default_random_engine generator;
std::uniform_real_distribution<float> distribution(0.0, 1.0);


struct thread_data {
    Protocol *socket_protocol;
    std::vector<Array<int, float> *> arr_list;
    std::string int_format;
    std::string data_format;
};


void *send(void *thread_args) {
    // Provides a function that can be threaded that allows server and
    // client to interact.
    struct thread_data *args;
    args = (struct thread_data *) thread_args;

    Protocol *socket_protocol = args->socket_protocol;
    std::string int_format = args->int_format;
    std::string data_format = args->data_format;

    if (socket_protocol->socket_type.compare("server") == 0) {
        socket_protocol->plisten(HOST, PORT);
    } else {
        int max_tries = 100;
        int sleep_ms = 100;

        for (int i=0; i<max_tries; i++) {
            try {
                socket_protocol->pconnect(HOST, PORT);
                break;
            } catch (const char *msg) {
                std::cout << "Connection refused " << i+1 << " of " << max_tries << std::endl;
                std::this_thread::sleep_for(std::chrono::milliseconds(sleep_ms));
            }
            if (i == max_tries - 1) {
                throw "Couldn't connect with server!";
            }
        }
    }

    for (int i=0; i<args->arr_list.size(); i++) {
        socket_protocol->psend(
            args->arr_list[i],
            int_format,
            data_format
        );
    }

    return 0;
}

void *receive(void *thread_args) {
    // Provides a function that can be threaded that allows server and
    // client to interact.
    struct thread_data *args;
    args = (struct thread_data *) thread_args;

    Protocol *socket_protocol = args->socket_protocol;
    std::string int_format = args->int_format;
    std::string data_format = args->data_format;

    if (socket_protocol->socket_type.compare("server") == 0) {
        socket_protocol->plisten(HOST, PORT);
    } else {
        int max_tries = 100;
        int sleep_ms = 100;

        for (int i=0; i<max_tries; i++) {
            try {
                socket_protocol->pconnect(HOST, PORT);
                break;
            } catch (const char *msg) {
                std::cout << "Connection refused " << i+1 << " of " << max_tries << std::endl;
                std::this_thread::sleep_for(std::chrono::milliseconds(sleep_ms));
            }
            if (i == max_tries - 1) {
                throw "Couldn't connect with server!";
            }
        }
    }

    for (int i=0; i<args->arr_list.size(); i++) {
        socket_protocol->preceive(
            args->arr_list[i],
            int_format,
            data_format
        );
    }

    return 0;
}

int test_send_client_to_server(int arr_list_len, int connections,
    std::string int_format, std::string data_format) {
    // Test data can be sent from client to server correctly.
    int verbose;
    if ((connections == 1) && (arr_list_len == 1)) {
        verbose = 1;
    } else {
        verbose = 0;
    }

    Protocol sp("server", verbose);
    Protocol cp("client", verbose);

    for (int i=0; i<connections; i++) {
        // Create the vector of arrays to send.
        std::vector<Array<int, float> *> arr_list_send;
        std::vector<Array<int, float> *> arr_list_recv;

        for (int j=0; j<arr_list_len; j++) {
            arr_list_recv.push_back(new Array<int, float>);

            arr_list_send.push_back(new Array<int, float>);
            arr_list_send.back()->size = 120;
            arr_list_send.back()->dim = 3;
            arr_list_send.back()->allocate();
            arr_list_send.back()->shape[0] = 4;
            arr_list_send.back()->shape[1] = 5;
            arr_list_send.back()->shape[2] = 6;

            for (int k=0; k<120; k++) {
                arr_list_send.back()->data[k] = distribution(generator);
            }
        }

        pthread_t threads[2];
        pthread_attr_t attr;
        struct thread_data td[2];

        // Initialise and set thread joinable.
        pthread_attr_init(&attr);
        pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

        td[0].socket_protocol = &sp;
        td[0].arr_list = arr_list_recv;
        td[0].int_format = int_format;
        td[0].data_format = data_format;

        td[1].socket_protocol = &cp;
        td[1].arr_list = arr_list_send;
        td[1].int_format = int_format;
        td[1].data_format = data_format;

        pthread_create(
            &threads[0],
            &attr,
            receive,
            (void *) &td[0]
        );
        pthread_create(
            &threads[1],
            &attr,
            send,
            (void *) &td[1]
        );

        // Don't understand why but this waits for threads to finish.
        void *status;
        pthread_attr_destroy(&attr);
        pthread_join(threads[0], &status);
        pthread_join(threads[1], &status);

        // Close current protocols.
        sp.pclose();
        cp.pclose();

        // Make sure the array lists are the same.
        for (int j=0; j<arr_list_len; j++) {
            for (int k=0; k<120; k++) {
                assert(arr_list_send[j]->data[k] == arr_list_recv[j]->data[k]);
            }
        }
    }

    pthread_exit(NULL);


    return 0;
}

int main() {
    test_send_client_to_server(1, 1, "int32", "float32");
    std::cout << "Hello!" << std::endl;

    return 0;
}
