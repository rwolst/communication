#include <iostream>
#include "protocol.hpp"


int main(int argc, char *argv[]) {
    // Declare the protocol.
    std::string socket_type;
    if (argc == 1) {
        std::cout << "No socket type provided so using server." << std::endl;
        socket_type = "server";
    } else {
        std::cout << "Using protocol type " << argv[1] << "." << std::endl;
        socket_type = argv[1];
    }
    Protocol cp(socket_type, 1);

    std::cout << "Total chunk bytes: " << cp.CHUNK_BYTES << std::endl;
    std::cout << "Socket Type: " << cp.socket_type << std::endl;
    std::cout << "Verbose: " << cp.verbose << std::endl;

    if (cp.socket_type.compare("server") == 0) {
        try {
            cp.plisten("127.0.0.1", 65432);
        } catch (const char *msg) {
            std::cerr << msg << std::endl;
        }

        // Now send some random data (5 x 2 array).
        if (!strcmp(argv[3], "float32") || !strcmp(argv[3], "float64")) {
            Array<int, float> arr(10, 2);
            arr.allocate();

            arr.shape[0] = 5;
            arr.shape[1] = 2;

            for (int i=0; i<10; i++) {
                arr.data[i] = i + i*0.1;
            }
            cp.psend(&arr, argv[2], argv[3]);
        } else {
            Array<int, int> arr(10, 2);
            arr.allocate();

            arr.shape[0] = 5;
            arr.shape[1] = 2;

            for (int i=0; i<10; i++) {
                arr.data[i] = i;
            }
            cp.psend(&arr, argv[2], argv[3]);
        }
    } else {
        try {
            cp.pconnect("127.0.0.1", 65432);
        } catch (const char *msg) {
            std::cerr << msg << std::endl;
        }

        // Now receive some random data.
        if (!strcmp(argv[3], "float32") || !strcmp(argv[3], "float64")) {
            Array<int, float> arr(-1, -1);
            cp.preceive(&arr, argv[2], argv[3]);
        } else {
            Array<int, int> arr(-1, -1);
            cp.preceive(&arr, argv[2], argv[3]);
        }
    }

    cp.pclose();

    return 0;
}
