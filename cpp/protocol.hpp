// Copyright 2019 <Rob>

#ifndef CPP_PROTOCOL_HPP_
#define CPP_PROTOCOL_HPP_

#include <iostream>
#include <cstring>


// Useful functions.
int is_system_le();

// Array class.
template <class int_T, class data_T>
class Array {
 public:
    // Fields.
    int_T size;
    int_T dim;
    int_T *shape;
    data_T *data;

    // Methods.
    Array() {};
    Array(int size, int dim);
    ~Array(void);

    int allocate(void);
};

// Protocol class.
class Protocol {
 public:
    // Class constants.
    static const int CHUNK_BYTES = 1024;

    // Fields.
    std::string socket_type;
    int verbose;

    // Methods.
    Protocol(std::string socket_type, int verbose);
    ~Protocol(void);

    int plisten(std::string host, int port);
    int pconnect(std::string host, int port);
    int pclose();

    template <class int_T, class data_T>
    int psend(
        Array<int_T, data_T> *arr,
        std::string int_format,
        std::string data_format
    );

    template <class int_T, class data_T>
    int preceive(
        Array<int_T, data_T> *arr,
        std::string int_format,
        std::string data_format
    );

 private:
    // Fields.
    int sock;
    int conn;

    // Methods.
    template <class int_T, class data_T>
    int psend_tcp(
        Array<int_T, data_T> *arr,
        std::string int_format,
        std::string data_format
    );

    template <class int_T, class data_T>
    int preceive_tcp(
        Array<int_T, data_T> *arr,
        std::string int_format,
        std::string data_format
    );
};

#endif  /* CPP_PROTOCOL_HPP_ */
