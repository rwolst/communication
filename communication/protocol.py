"""Defines classes that can be used for sending data between the simulator and
outside processes. Communication is done using TCP.

Note that communication is done by passing arrays in the following order:

    i) dim: Integer containing the dimension of the array.
    ii) shape: Array of shape (dim,) of integers representing shape of array.
    iii) data: Array of shape (*shape) of floats containing array data.

This means that the receiving program must first know that the sending program
wants to send data. This is accomplished by reading messages from RabbitMQ.

The data transfer can be tested by running in one shell
    python3 protocol SERVER {protocol_type}
and in another
    python3 protocol CLIENT {protocol_type}
where {protocol_type} is either UDP or TCP.
"""

import socket
from struct import pack, unpack

import numpy as np

from contexttimer import Timer


class Protocol():
    """Defines send and receive methods for server and client protocols."""
    # Send data in chunks of 1024 bytes.
    CHUNK_BYTES = 1024

    # Dictionary for keeping type declarations consistent between struct and
    # numpy.
    formats = {
        'int32': {'BYTES': 4, 'NUMPY': '<i4', 'STRUCT': '<i'},
        'int64': {'BYTES': 8, 'NUMPY': '<i8', 'STRUCT': '<q'},
        'float32': {'BYTES': 4, 'NUMPY': '<f4', 'STRUCT': '<f'},
        'float64': {'BYTES': 8, 'NUMPY': '<f8', 'STRUCT': '<d'},
    }

    def __init__(self, socket_type, verbose):
        """Define whether to use UDP or TCP.
        Inputs:
            socket_type: server or client.
            verbose: Boolean whether to display verbose output.
        """
        if socket_type not in ['server', 'client']:
            raise Exception("socket_type must be in:\n\t%s\ngot\n\t%s"
                            % (['server', 'client'], socket_type))

        self.socket_type = socket_type
        self.verbose = verbose

        # Define future attributes to be set.
        self.socket = None
        self.conn = None
        self.addr = None

    def listen(self, host, port):
        """Creates an actual socket and gets it to bind and listen.

        Inputs:
            host: Server IP address.
            port: Server port number.
        """
        if self.socket_type != 'server':
            raise Exception("Can only listen with server sockets.")

        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.socket.bind((host, port))
        self.socket.listen(1)

        # Accept a connection.
        (self.conn, self.addr) = self.socket.accept()

    def connect(self, host, port):
        """Connect to an actual socket.

        Inputs:
            host: Server IP address.
            port: Server port number.
        """
        if self.socket_type != 'client':
            raise Exception("Can only connect with client sockets.")

        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect((host, port))

    def close(self):
        """Closes a created server or client socket."""
        if self.socket_type == 'server':
            self.socket.close()
            self.socket = None
        elif self.socket_type == 'client':
            self.socket.shutdown(socket.SHUT_WR)
            self.socket.close()
            self.socket = None

    def send(self, arr, int_format):
        """Send a Numpy array over the socket.

        The protocol of sending to the client is roughly as follows:
            i)   SEND integer dimension of arr.
            ii)  SEND integer array of shape of arr.
            iii) SEND float data within arr.
            iv)  RECEIVE integer acknowledging completion.
        For integer and float sizes see the self.formats constant values.

        In the send case we don't have to ensure the socket gets closed as
        we can send multiple objects with it. However when shutting down the
        client it must get called manually.

        Inputs:
            arr: A Numpy array to send.
        """
        ack = self._send_TCP(arr, int_format)

        assert ack == 0

        return 0

    def _send_TCP(self, arr, int_format):
        """Send a Numpy array over a socket using TCP protocol. It can be sent
        from the server to the client but also from the client to the server.

        We must already be connected (use `connect` method) to a socket to do
        this."""
        "pdb"
        if self.socket_type == 'server':
            # We use the connection for sending.
            conn = self.conn
            addr = self.addr
        elif self.socket_type == 'client':
            # We use socket itself for sending.
            conn = self.socket
            addr = None

        # i) Send integer dimension.
        with Timer() as t:
            dim = arr.ndim
            data = pack(self.formats[int_format]['STRUCT'], dim)
            conn.sendall(data)

            if self.verbose:
                print('Sent Dimension: %s\n\t%ss' % (dim, t.elapsed))

        # ii) Send integer shape (total of `dim` integers).
        with Timer() as t:
            # Note we use big endian as sending over network.
            shape = np.array(
                arr.shape,
                dtype=self.formats[int_format]['NUMPY']
            )
            conn.sendall(shape.tobytes())

            if self.verbose:
                print('Sent Shape: %s\n\t%ss' % (shape, t.elapsed))

        # iii) Send float data within arr (total of prod(shape) floats).
        with Timer() as t:
            conn.sendall(arr.tobytes())

            if self.verbose:
                if arr.size < 20:
                    print('Sent Array:\n%s\n\t%ss' % (arr, t.elapsed))
                else:
                    print('Sent Array:\n\tarr[0]=%s\tarr[-1]=%s\n\t%ss'
                          % (arr.ravel()[0], arr.ravel()[-1], t.elapsed))

        # iv) Receive acknowledgement.
        #import pdb; pdb.set_trace()
        with Timer() as t:
            ack = conn.recv(1)
            assert ack == b'\x00'

            if self.verbose:
                print('Recieved acknowledgement\n\t%ss' % (t.elapsed,))

        return 0

    def receive(self, int_format, arr_format):
        """Receives a Numpy array over the socket. The socket can be a client
        or a server.

        The protocol of sending to the client is roughly as follows:
            iii) RECEIVE integer dimension of arr.
            iv)  RECEIVE integer array of shape of arr.
            v)   RECEIVE float data within arr.
            vi)  SEND integer acknowledging completion.
        For integer and float sizes see the self.formats dictionary.
        """
        arr = self._receive_TCP(int_format, arr_format)

        return arr

    def _receive_TCP(self, int_format, arr_format):
        """Receive a Numpy array over a socket using TCP protocol."""
        if self.socket_type == 'server':
            # We use the connection for receiving.
            conn = self.conn
            addr = self.addr
        elif self.socket_type == 'client':
            # We use socket itself for receiving.
            conn = self.socket
            addr = None

        # i) Receive integer dimension.
        with Timer() as t:
            data = conn.recv(self.formats[int_format]['BYTES'])
            (dim,) = unpack(self.formats[int_format]['STRUCT'], data)

            if self.verbose:
                print('From addr: %s\n\tDimension: %s\n\t%ss'
                      % (addr, dim, t.elapsed))

        # ii) Receive integer shape (total of `dim` integers).
        #import pdb; pdb.set_trace()
        with Timer() as t:
            shape = np.empty([dim], dtype=self.formats[int_format]['NUMPY'])
            self._receive_into(conn, shape, arr_format)

            if self.verbose:
                print('From addr: ?\n\tShape: %s\n\t%ss' % (shape, t.elapsed))

        # iii) Receive float or int data within arr (total of prod(shape)).
        with Timer() as t:
            arr = np.empty(shape, dtype=self.formats[arr_format]['NUMPY'])
            self._receive_into(conn, arr, arr_format)

            if self.verbose:
                if arr.size < 20:
                    print('From addr: ?\n\tData:\n%s\n\t%ss' % (arr, t.elapsed))
                else:
                    print('From addr: ?\n\tData:\n\tarr[0]=%s\tarr[-1]=%s\n\t%ss'
                          % (arr.ravel()[0], arr.ravel()[-1], t.elapsed))

        # iv) No acknowledgement necessary for TCP however send back own
        #     acknowledgement.
        with Timer() as t:
            conn.sendall(b'\x00')
            if self.verbose:
                print('Acknowledgement sent\n\t%ss' % t.elapsed)

        return arr

    @classmethod
    def _receive_into(cls, socket_conn, arr, arr_format):
        """Recieves data from a socket into a Numpy array arr.

        Inputs:
            socket_conn: A Python socket (UDP) or connection (TCP) object.
            arr: A Numpy array with shape defined.
        """
        # Get the total bytes we expect to receive from size of arr.
        exp_bytes = arr.nbytes
        dtype_bytes = arr.dtype.itemsize
        chunk_size = cls.CHUNK_BYTES // dtype_bytes

        # Continually read from the socket until the total received bytes is
        # equal to the expected bytes.
        total_rec_bytes = 0
        i = 0
        while total_rec_bytes < exp_bytes:
            # Get start and end points we are reading into array.
            start = i * chunk_size
            end = (i+1) * chunk_size

            # Receive the data.
            rec_bytes = socket_conn.recv_into(arr.ravel()[start:end])

            # Add to the total received byte accumulator and move to next
            # chunk.
            total_rec_bytes += rec_bytes
            i += 1

        # Return succesful completion.
        return 0
