"""Test that we can

    i)   Connect a client and server using TCP.
    ii)  Send array from client to server.
    iii) Send array from server to client.
    iv)  Send multiple arrays from client to server over one connection.
    v)   Send multiple arrays from server to client over one connection.
    vi)  Send multiple arrays from client to server over multiple connections
         i.e. close connections and re-open new one.
    vii) Send multiple arrays from server to client over multiple connections
         i.e. close connections and re-open new one.
"""

import queue
import threading
import time

import numpy as np
import numpy.testing as npt
import pytest

from communication.protocol import Protocol


# Define the host and port to communicate over.
HOST = '127.0.0.1'
PORT = 65432


def send(socket_protocol, arr_list, int_format, arr_format):
    """Provides a function that can be threaded that allows server and client
    to interact."""
    max_tries = 100
    sleep = 0.1

    if socket_protocol.socket_type == 'server':
        socket_protocol.listen(HOST, PORT)
    elif socket_protocol.socket_type == 'client':
        for i in range(max_tries):
            try:
                socket_protocol.connect(HOST, PORT)
                break
            except ConnectionRefusedError:
                print(f"Connection refused {i} of {max_tries}, sleeping"
                      f" {sleep}s.")
                time.sleep(sleep)

            if i == max_tries - 1:
                raise Exception("Couldn't connect with server!")


    for arr in arr_list:
        socket_protocol.send(arr, int_format)


def receive(socket_protocol, arr_list_len, int_format, arr_format, q):
    """Provides a function that can be threaded that allows server and client
    to interact."""
    max_tries = 100
    sleep = 0.1

    if socket_protocol.socket_type == 'server':
        socket_protocol.listen(HOST, PORT)
    elif socket_protocol.socket_type == 'client':
        for i in range(max_tries):
            try:
                socket_protocol.connect(HOST, PORT)
                break
            except ConnectionRefusedError:
                print(f"Connection refused {i} of {max_tries}, sleeping"
                      f" {sleep}s.")
                time.sleep(sleep)

            if i == max_tries - 1:
                raise Exception("Couldn't connect with client!")

    for i in range(arr_list_len):
        arr = socket_protocol.receive(int_format, arr_format)
        q.put(arr)


@pytest.mark.parametrize(
    "arr_list_len,connections,int_format,arr_format",
    [
        (1, 1, 'int32', 'int32'),
        (1, 1, 'int32', 'float64'),
        (5, 1, 'int64', 'int32'),
        (5, 1, 'int32', 'float64'),
        (10, 1, 'int32', 'int32'),
        (10, 1, 'int32', 'float64'),
        (1, 50, 'int32', 'int64'),
        (1, 50, 'int32', 'float64'),
        (5, 50, 'int64', 'int64'),
        (5, 50, 'int64', 'float64'),
        (10, 50, 'int32', 'int32'),
        (10, 50, 'int32', 'float64'),
    ]
)
def test_send_client_to_server(arr_list_len, connections, int_format, arr_format):
    # Define server and client object.
    if (connections == 1) and (arr_list_len == 1):
        verbose = True
    else:
        verbose = False

    sp = Protocol('server', verbose=verbose)
    cp = Protocol('client', verbose=verbose)

    for i in range(connections):
        arr_list = [
            np.random.rand(4, 5, 6) \
                .astype(Protocol.formats[arr_format]['NUMPY'])
            for _ in range(arr_list_len)
        ]
        q = queue.Queue()

        sp_thread = threading.Thread(
            target=receive,
            args=(sp, arr_list_len, int_format, arr_format, q)
        )
        cp_thread = threading.Thread(
            target=send,
            args=(cp, arr_list, int_format, arr_format)
        )

        sp_thread.start()
        cp_thread.start()

        sp_thread.join()
        cp_thread.join()

        # First append to avoid crashing the connection on failure.
        arr_received = []
        for i in range(arr_list_len):
            arr_received.append(q.get())

        sp.close()
        cp.close()

        for i in range(arr_list_len):
            npt.assert_array_equal(arr_list[i], arr_received[i])


@pytest.mark.parametrize(
    "arr_list_len,connections,int_format,arr_format",
    [
        (1, 1, 'int32', 'int32'),
        (1, 1, 'int32', 'float64'),
        (5, 1, 'int64', 'int32'),
        (5, 1, 'int32', 'float64'),
        (10, 1, 'int32', 'int32'),
        (10, 1, 'int32', 'float64'),
        (1, 50, 'int32', 'int64'),
        (1, 50, 'int32', 'float64'),
        (5, 50, 'int64', 'int64'),
        (5, 50, 'int64', 'float64'),
        (10, 50, 'int32', 'int32'),
        (10, 50, 'int32', 'float64'),
    ]
)
def test_send_server_to_client(arr_list_len, connections, int_format, arr_format):
    # Define server and client object.
    if (connections == 1) and (arr_list_len == 1):
        verbose = True
    else:
        verbose = False

    sp = Protocol('server', verbose=verbose)
    cp = Protocol('client', verbose=verbose)

    for i in range(connections):
        arr_list = [
            np.random.rand(4, 5, 6) \
                .astype(Protocol.formats[arr_format]['NUMPY'])
            for _ in range(arr_list_len)
        ]
        q = queue.Queue()

        sp_thread = threading.Thread(
            target=send,
            args=(cp, arr_list, int_format, arr_format)
        )
        cp_thread = threading.Thread(
            target=receive,
            args=(sp, arr_list_len, int_format, arr_format, q)
        )

        sp_thread.start()
        cp_thread.start()

        sp_thread.join()
        cp_thread.join()

        # First append to avoid crashing the connection on failure.
        arr_received = []
        for i in range(arr_list_len):
            arr_received.append(q.get())

        sp.close()
        cp.close()

        for i in range(arr_list_len):
            npt.assert_array_equal(arr_list[i], arr_received[i])
